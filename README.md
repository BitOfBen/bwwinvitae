# Cohort Analysis 
### Author: Ben Wilder

## Setup

This application relies on environment variables to pull configuration information.
The following env vars are expected to be set:

COHORT_LENGTH_DAYS - integer representing the number of days used for cohort creation

COHORT_SIGNUP_DAYS - integer representing the number of days to be used to track since cohort signup

DEFAULT_TIMEZONE - a string dictating the timezone to be used for dates and times.

NUMBER_OF_COHORTS - integer representing the max number of cohorts to be processed

### Dependencies
Dependencies are managed with the `requirements.txt` file. 

To install from the file run `pip install -r requirements.txt`.

## Build

Execute `docker build -f cohort.Dockerfile .` to build the image.
Tag the image appropriately when building an image for promotion i.e. `-t bwwinvitate:1.0`

## Execution 
The program can be executed by running the following command on the docker image:

`docker run -v {output_dir}:/output {image_name}`

WHERE:

`output_dir` is the name of the directory where you would like the output csv to be placed.

`image_name` is the name of the image available locally or in an image repo.

### Environment Variables

All environment variables are built with the docker file. However, the env vars can be overwritten either in the docker 
build command or when calling `docker run`.

Example:

`docker run -v ~/Documents:/output --env NUMBER_OF_COHORTS=1 {image_name}` 

This example would run the analysis with a cohort size of 1.

## Testing

execute unit tests with pytest.