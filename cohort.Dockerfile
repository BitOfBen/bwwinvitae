FROM python:3

RUN python --version

COPY requirements.txt .


RUN pip install -r requirements.txt

COPY src src/
COPY data data/
RUN mkdir output

ENV COHORT_LENGTH_DAYS=7
ENV COHORT_SIGNUP_DAYS=7
ENV DEFAULT_TIMEZONE=US/Pacific
ENV NUMBER_OF_COHORTS=8

CMD ["python", "-m", "src.cohort_analysis"]