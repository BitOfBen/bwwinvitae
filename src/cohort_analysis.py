import pandas

from src.config.config import Config
from src.helpers.analysis_helper import AnalysisHelper
from src.helpers.reader_helper import ReaderHelper
from src.utils.csv_reader import CsvReader

from src.utils.csv_writer import CsvWriter
from src.utils.join_dataframes import JoinDataframes


def get_cohort(customer_data, cohort_start):
    customer_data = ((customer_data.dt.date - cohort_start) % 7).dt.days
    return customer_data


# TODO: break this out into better defined class(es)
def execute_analysis():
    config = Config()
    reader = CsvReader()
    writer = CsvWriter(config)

    reader_helper = ReaderHelper(config, reader)
    analysis_helper = AnalysisHelper(config, writer)

    customer_data = reader_helper.read_customers("data/customers.csv")
    customer_orders_data = reader_helper.read_orders("data/orders.csv")

    cohort_start = analysis_helper.get_cohort_start(customer_data)
    customer_data["cohort"] = customer_data.apply(
        lambda row: analysis_helper.build_cohorts_dates(
            row["created"].date(),
            cohort_start,
            config.cohort_config().cohort_length_days(),
        ),
        axis=1,
    )

    cohort_solution = JoinDataframes.join_left(
        customer_data, "id", customer_orders_data, "user_id"
    ).dropna()

    cohort_solution["order_cohort"] = cohort_solution.apply(
        lambda row: analysis_helper.build_cohorts_days(
            row["created_y"].date(),
            row["cohort"],
            config.cohort_config().cohort_signup_days(),
        ),
        axis=1,
    )

    cohort_solution["order_cohort_dates"] = cohort_solution.apply(
        lambda row: analysis_helper.build_cohorts_dates(
            row["created_y"].date(),
            cohort_start,
            config.cohort_config().cohort_signup_days(),
        ),
        axis=1,
    )

    last_cohort = analysis_helper.find_last_cohort(cohort_start)

    cohort_solution = cohort_solution[cohort_solution["cohort"] < last_cohort]
    cohort_solution = cohort_solution[
        cohort_solution["order_cohort_dates"] < last_cohort
    ]
    cohort_solution.sort_values("order_cohort_dates")
    cohort_solution = cohort_solution.drop(
        [
            "created_x",
            "created_y",
            "id_x",
            "order_number",
            "user_id",
            "order_cohort_dates",
        ],
        axis=1,
    )
    cohort_solution.rename(columns={"id_y": "users"}, inplace=True)
    output_table = cohort_solution.pivot_table(
        index="cohort", columns="order_cohort", aggfunc=lambda x: x.nunique()
    )
    print(output_table)
    analysis_helper.save_analysis("output/final_output.csv", output_table)

# Honestly, I probably shouldn't have built a config and just gone with CLI for arguments and built some kind of CLI here.
execute_analysis()
