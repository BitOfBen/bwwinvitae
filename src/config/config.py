import os
from builtins import int


class Config:
    def __init__(self):
        self.__cohort_config = CohortConfig()

    def cohort_config(self):
        return self.__cohort_config


class CohortConfig:
    def __init__(self):
        self.__cohort_length_days = int(os.getenv("COHORT_LENGTH_DAYS", 7))
        self.__cohort_signup_days = int(os.getenv("COHORT_SIGNUP_DAYS", 7))
        self.__default_timezone = os.getenv("DEFAULT_TIMEZONE", "US/Pacific")
        self.__number_of_cohorts = int(os.getenv("NUMBER_OF_COHORTS", 8))

    def cohort_length_days(self):
        return self.__cohort_length_days

    def cohort_signup_days(self):
        return self.__cohort_signup_days

    def default_timezone(self):
        return self.__default_timezone

    def number_of_cohorts(self):
        return self.__number_of_cohorts
