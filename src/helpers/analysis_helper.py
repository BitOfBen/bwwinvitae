import datetime
from pandas import DataFrame
from src.config.config import Config
from src.utils.sink import Sink


# TODO: Add unit tests for each function
class AnalysisHelper:
    def __init__(self, config: Config, out_sink: Sink):
        self.__config = config
        self.__out_sink = out_sink

    def get_cohort_start(self, customers: DataFrame):
        return customers["created"].min().date()

    # Add the dates for a given cohort
    def build_cohorts_dates(
            self, actual_date: datetime, cohort_start: datetime, cohort_length: int
    ):
        total_days = actual_date - cohort_start
        days = total_days.days % cohort_length
        first_cohorts = actual_date - datetime.timedelta(days=days)
        return first_cohorts

    def build_cohorts_days(
            self, actual_date: datetime, cohort_start: datetime, cohort_length: int
    ):
        total_days = actual_date - cohort_start
        difference = total_days.days % cohort_length
        first_day = total_days.days - difference
        # Hacky solution to sorting issue. Columns are out of order otherwise
        first_day_string = first_day if first_day > 9 else f"0{first_day}"
        output = f"{first_day_string} - {first_day+cohort_length-1}"
        return output

    def find_last_cohort(self, cohort_start: datetime.date):
        final_cohort = cohort_start + datetime.timedelta(
            days=(
                    self.__config.cohort_config().number_of_cohorts()
                    * self.__config.cohort_config().cohort_length_days()
            )
        )
        return final_cohort

    def save_analysis(self, output_name: str, data: DataFrame):
        self.__out_sink.save_file(output_name, data)
