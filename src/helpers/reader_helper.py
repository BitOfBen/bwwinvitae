import pandas

from pandas import DataFrame


class ReaderHelper:
    def __init__(self, config, reader):
        self.__config = config
        self.__reader = reader

    def read_customers(self, file_name):
        output = self.__reader.read_dataframe(file_name)
        output["created"] = self.__format_dates(output["created"])
        return output

    def read_orders(self, file_name):
        output = self.__reader.read_dataframe(file_name)
        output["created"] = self.__format_dates(output["created"])
        return output

    def __format_dates(self, date_list: DataFrame):
        # TODO: This is a little weird, something needs to be fixed. too many casts to datetime
        date_list = pandas.to_datetime(date_list)
        timezone_str = str(self.__config.cohort_config().default_timezone())
        date_list = date_list.dt.tz_localize("UTC").dt.tz_convert(timezone_str)
        date_list = pandas.to_datetime(date_list)
        return date_list
