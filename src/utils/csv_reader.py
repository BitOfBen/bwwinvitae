import pandas


class CsvReader:
    @staticmethod
    def read_lines(file_name):
        with open(file_name, "r") as f:
            lines = [line.split() for line in f]
        return lines

    @staticmethod
    def read_dataframe(file_name):
        return pandas.read_csv(file_name)
