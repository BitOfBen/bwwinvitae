from src.utils.sink import Sink


class CsvWriter(Sink):
    def __init__(self, config):
        super().__init__(config)

    def save_file(self, file_name, raw_data):
        raw_data.to_csv(file_name)
