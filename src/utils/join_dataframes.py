import pandas as pd


class JoinDataframes:
    @staticmethod
    def join_left(df1, df1_col, df2, df2_col):
        result = pd.merge(df1, df2, how="left", left_on=[df1_col], right_on=[df2_col])
        return result
