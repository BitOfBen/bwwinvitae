from datetime import datetime
from unittest.mock import MagicMock

from src.helpers.analysis_helper import AnalysisHelper
from tests.unit_tests.test_data.test_data import TestData


# Todo: convert to class and add config creation before each
def test_get_cohort_start():
    # Setup
    config = MagicMock()

    cohort_config = MagicMock()
    cohort_config.default_timezone.return_value = "US/Pacific"
    cohort_config.cohort_length_days.return_value = 7
    cohort_config.cohort_signup_days.return_value = 7
    cohort_config.number_of_cohorts.return_value = 8

    config.cohort_config.return_value = cohort_config

    out_mock = MagicMock()
    customers = TestData.customer_data_frame()
    helper = AnalysisHelper(config, out_mock)
    # Execute
    start = helper.get_cohort_start(customers)

    # Validate
    assert start == datetime.strptime("2015-02-06 06:11:22", TestData.fmt).date()


def test_build_cohorts_dates_ok():
    # Setup
    config = MagicMock()

    cohort_config = MagicMock()
    cohort_config.default_timezone.return_value = "US/Pacific"
    cohort_config.cohort_length_days.return_value = 7
    cohort_config.cohort_signup_days.return_value = 7
    cohort_config.number_of_cohorts.return_value = 8

    config.cohort_config.return_value = cohort_config

    out_mock = MagicMock()
    helper = AnalysisHelper(config, out_mock)

    actual_date = datetime.strptime("2015-07-06 06:11:22", TestData.fmt).date()
    cohort_start = datetime.strptime("2015-02-03 06:11:22", TestData.fmt).date()

    # Execute
    cohort_dates = helper.build_cohorts_dates(actual_date, cohort_start, 7)

    # Validate
    assert cohort_dates == datetime.strptime("2015-06-30 06:11:22", TestData.fmt).date()


def test_build_cohorts_days_ok():
    # Setup
    config = MagicMock()

    cohort_config = MagicMock()
    cohort_config.default_timezone.return_value = "US/Pacific"
    cohort_config.cohort_length_days.return_value = 7
    cohort_config.cohort_signup_days.return_value = 7
    cohort_config.number_of_cohorts.return_value = 8

    config.cohort_config.return_value = cohort_config

    out_mock = MagicMock()
    helper = AnalysisHelper(config, out_mock)

    actual_date = datetime.strptime("2015-03-06 06:11:22", TestData.fmt).date()
    cohort_start = datetime.strptime("2015-02-03 06:11:22", TestData.fmt).date()

    # Execute
    cohort_days = helper.build_cohorts_days(actual_date, cohort_start, 7)

    # Validate
    assert cohort_days == "28 - 34"


def test_find_last_cohort():
    # Setup
    config = MagicMock()

    cohort_config = MagicMock()
    cohort_config.default_timezone.return_value = "US/Pacific"
    cohort_config.cohort_length_days.return_value = 7
    cohort_config.cohort_signup_days.return_value = 7
    cohort_config.number_of_cohorts.return_value = 8

    config.cohort_config.return_value = cohort_config

    out_mock = MagicMock()
    helper = AnalysisHelper(config, out_mock)

    cohort_start = datetime.strptime("2015-02-03 06:11:22", TestData.fmt).date()

    # Execute
    cohort_days = helper.find_last_cohort(cohort_start)

    # Validate
    assert cohort_days == datetime.strptime("2015-03-31 06:11:22", TestData.fmt).date()
