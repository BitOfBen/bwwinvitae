from src.utils.csv_reader import CsvReader


def test_reader_lists():
    # Setup
    file_name = "data/customers.csv"
    reader = CsvReader()

    # Execute
    lines = reader.read_lines(file_name)

    # Validate
    assert len(lines) == 25717


def test_reader_df():
    # Setup
    file_name = "data/customers.csv"
    reader = CsvReader()

    # Execute
    df = reader.read_dataframe(file_name)

    # Validate
    assert df["id"].iloc[66] == 35477
    assert df["created"].iloc[13] == "2015-07-03 22:57:23"
