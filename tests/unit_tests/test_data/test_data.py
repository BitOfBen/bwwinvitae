import numpy as np
import pandas
from datetime import datetime


class TestData:
    fmt = "%Y-%m-%d %H:%M:%S"

    @staticmethod
    def customer_data_frame():
        return pandas.DataFrame(
            np.array(
                [
                    [1, datetime.now()],
                    [2, datetime.strptime("2015-07-11 12:11:22", TestData.fmt)],
                    [3, datetime.strptime("2015-07-15 03:11:22", TestData.fmt)],
                    [4, datetime.strptime("2015-02-06 06:11:22", TestData.fmt)],
                    [5, datetime.strptime("2015-07-09 14:11:22", TestData.fmt)],
                ]
            ),
            columns=["id", "created"],
        )

    @staticmethod
    def order_data_frame():
        return pandas.DataFrame(
            np.array(
                [
                    [1, 4, 1, "2015-07-03 22:01:11"],
                    [2, 2, 2, "2015-07-03 22:11:23"],
                    [3, 6, 4, "2015-07-24 22:01:11"],
                    [4, 1, 1, "2015-07-23 22:11:23"],
                ]
            ),
            columns=["id", "order_number", "user_id", "created"],
        )
